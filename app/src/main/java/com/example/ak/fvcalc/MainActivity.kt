package com.example.ak.fvcalc

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.zysk.*
import android.graphics.Typeface
import android.widget.CompoundButton
import android.widget.ToggleButton
import kotlinx.android.synthetic.main.zysk.view.*
import android.content.DialogInterface
import android.content.DialogInterface.BUTTON_NEUTRAL
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import org.jetbrains.annotations.Nullable


class MainActivity : AppCompatActivity() {

    var menu:String = "wysokoscFv"

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.main_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item:MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.calc_1) {
            menu = "zyskZFv"
            tekst.setText("Podaj kwotę netto z fv")
            clear()
        }
        if (id == R.id.calc_2) {
            menu = "wysokoscFv"
            tekst.setText("Podaj oczekiwaną kwotę na czysto")
            clear()
        }
        if (id == R.id.czysc) {
            clear()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.zysk)

        var roznicaZusu:String = "0"
        val alertDialog = AlertDialog.Builder(this@MainActivity).create()

        onoff.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                //przypisanie do zmiennej w celu dodania do spolecznego
                CheckYes.setChecked(false)
                roznicaZusu = "15.44"
                val valueZ = "319.94"
                txtHealth.setText(valueZ)
                val valueS = "184.72"
                txtCommunity.setText(valueS)
                val valueW = "0"
                txtWork.setText(valueW)
                val valueCosts = "0"
                txtCosts.setText(valueCosts)
            } else {
                //przypisanie do zmiennej w celu dodania do spolecznego
                CheckYes.setChecked(false)
                roznicaZusu = "65.31"
                val valueZ = "319.94"
                txtHealth.setText(valueZ)
                val valueS = "781.60"
                txtCommunity.setText(valueS)
                val valueW = "65.31"
                txtWork.setText(valueW)
                val valueCosts = "0"
                txtCosts.setText(valueCosts)
            }
        }

        CheckYes.setOnCheckedChangeListener { _, isChecked ->

            if(txtCommunity.text.toString() > "0") {
                val chorobowe:Float = txtCommunity.text.toString().toFloat()
                if (isChecked) {
                    val wynikspol = chorobowe + roznicaZusu.toFloat()
                    txtCommunity.setText(wynikspol.toString())
                } else {
                    val wynikspol = chorobowe - roznicaZusu.toFloat()
                    txtCommunity.setText(wynikspol.toString())
                }
            } else {
                alertDialog.setTitle("Błąd")
                alertDialog.setMessage("Wybierz NISKI lub WYSOKI ZUS")
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
                alertDialog.show()
            }
        }

        buCalcNetto.setOnClickListener {
            if(
                txtHealth.text.toString().trim().isNullOrBlank() ||
                txtNetto.text.toString().trim().isNullOrBlank()
                ) {

                alertDialog.setTitle("Błąd")
                alertDialog.setMessage("Wypełnij proszę dane")
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
                alertDialog.show()

            } else {
                liczenie(menu)
            }
        }
    }

    fun liczenie(menu:String) {
        val netto: Float = txtNetto.text.toString().toFloat()
        val zdrowotne: Float = txtHealth.text.toString().toFloat()
        val spoleczne: Float = txtCommunity.text.toString().toFloat()
        val fundusz: Float = txtWork.text.toString().toFloat()
        val koszta: Float = txtCosts.text.toString().toFloat()
        val percentValue = getResources().getStringArray(R.array.percentItemCodes)
        val procent: Float = percentValue[SpinnerPercent.getSelectedItemPosition()].toFloat()

        var text:String = ""
        val x = (netto - spoleczne - fundusz - koszta) * procent
        val obnizka = 46.33 +275.53
        val y = x - obnizka
        val oplaty = zdrowotne + spoleczne + fundusz
        if (menu == "zyskZFv") {
            val wynikf = netto - oplaty -koszta - y
            text = "Twój dochód po odliczeniu wszystkich podanych kosztów wyniesie " + String.format("%.2f", wynikf) + " zł"
        } else if (menu == "wysokoscFv") {
            val wynikf = netto + oplaty + koszta + x
            text = "Twój dochód po odliczeniu wszystkich podanych kosztów wyniósł $netto zł, powinieneś wystawić fakturę na " + String.format("%.2f", wynikf) + " zł netto"
        }

        val alertDialog = AlertDialog.Builder(this@MainActivity).create()
        alertDialog.setTitle("Wysokość Twojej faktury")
        alertDialog.setMessage(text)
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
        alertDialog.show()
    }

    fun clear() {
        CheckYes.setChecked(false)
        txtCommunity.setText(null)
        txtHealth.setText(null)
        txtCosts.setText(null)
        txtNetto.setText(null)
        txtWork.setText(null)
        SpinnerPercent.setSelection(0)
    }
}


